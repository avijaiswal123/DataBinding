package com.example.cfe.databinding.view;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.cfe.databinding.R;
import com.example.cfe.databinding.databinding.PostRowItemBinding;
import com.example.cfe.databinding.model.Post;

import java.util.List;

import static android.view.LayoutInflater.from;

public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.ViewHolder>{
    private List<Post> postList;
   // private LayoutInflater layoutInflater;
    private PostsAdapterListener listener;

    public PostsAdapter(List<Post> postList, PostsAdapterListener listener) {
        this.postList = postList;
        this.listener = listener;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater=from(parent.getContext());



        PostRowItemBinding binding= DataBindingUtil.inflate(layoutInflater,R.layout.post_row_item,parent,false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
         holder.binding.setPost(postList.get(position));
         holder.binding.thumbnail.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 if(listener !=null){
                     listener.onPostClicked(postList.get(position));
                 }
             }
         });

    }

    @Override
    public int getItemCount() {
        return postList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final PostRowItemBinding binding;
        public ViewHolder( PostRowItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
    public interface PostsAdapterListener {
        void onPostClicked(Post post);
    }
}
