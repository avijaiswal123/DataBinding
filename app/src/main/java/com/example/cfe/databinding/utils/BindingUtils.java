package com.example.cfe.databinding.utils;

import java.util.Locale;

public class BindingUtils {

    //used to convert value in human readable format like 55000 into 55k.
    public static String convertToSuffix(long count) {
        if (count < 1000) return "" + count;
        int exp = (int) (Math.log(count) / Math.log(1000));
        return String.format(Locale.ENGLISH,"%.1f%c",
                count / Math.pow(1000, exp),
                "kmgtpe".charAt(exp - 1));
    }
}
